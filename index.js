alert ("Hello, world!")

let firstName = "John"
let lastName = "Smith"
let age = 30
let hobbies = ["Biking", "Mountain Climbing", "Swimming"]
let workAddress = {
	houseNumber: 32,
	street: "Washington",
	city: "Lincoln",
	state: 'Nebraska'
}
let isMarried = null

function printUserInfo() {
	console.log(firstName + " " + lastName + " is " + age + " years of age.")
	console.log("This was printed inside of the function")
	console.log(hobbies)
	console.log("This was printed inside of the function")
	console.log(workAddress)
}

function returnFunction() {
	isMarried = true
	return ("The value of isMarried is: " + isMarried)
}

console.log("First Name: " + firstName)
console.log("Last Name: " + lastName)
console.log("Age: " + age)
console.log("Hobbies:")
console.log(hobbies)
console.log("Work Address:")
console.log(workAddress)
printUserInfo()
console.log(returnFunction())